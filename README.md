# Simple Flowline Model#

This repository is for flowline model created for the GEOL891 in Fall 2020. The flowline model is for a glacier growth and decay based on continuity equation and influx and outflux along with surface mass balance (SMB).
### What is this repository for? ###

* Tracks changes of the glacier flowline model from inception of the model to final version supporting sliding.

### How do I get set up? ###

* Anaconda distribution of Python 3.7.6
* Plotly for interactive plotting of the model with a interactive UI.
* Numpy, Matplotlib, Pandas, Scipy,



	
       				    ----------------------------------------
                          Inputs: (Gravity(g), Ice density(p), 
                                  Bed length(x), Bed slope,   
                                  Flow Parameters: n, A)      
                        ----------------------------------------
                                            |
                                            |
                                            V
                            ---------------------------------
                               Accumulation: (Uniform SMB)   
                            ---------------------------------
                                            |
                                            |
                                            V
                            ---------------------------------
                               Continuity Eqn: (dh/dt)        
                            ---------------------------------


### Deployment instructions
#### On the terminal:
   __python__ _flowlineModel_2.0.py_
   
### UI interface of the model
![Simple Flowline Model UI](https://bitbucket.org/sid07/numericalmodeling/raw/29f63412510e48160c5e20afe97224a14d2e3047/imgs/flowlineModelUI.png)