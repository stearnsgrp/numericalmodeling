#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#####################################
# Simple Flowline Model of a glacier#
#####################################

'''
@author: Siddharth Shankar

                        ########################################
                        # Inputs: (Gravity(g), Ice density(p), 
                                  Bed length(x), Bed slope, 
                                  Flow Parameters: n, A)
                        ########################################
                                            |
                                            |
                                            V
                            #################################
                               Accumulation: (Uniform SMB)   
                            #################################
                                            |
                                            |
                                            V
                            #################################
                               Continuity Eqn: (dh/dt)        
                            #################################

'''

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from numpy import diff
import scipy.integrate as integrate
from scipy.interpolate import make_interp_spline, BSpline
from scipy.signal import savgol_filter
from scipy.stats import norm

g = 9.81 # m/s^2
p = 917 # kg/m^3
x = 10000 # meters
n = 3
A = 1.7*10E-24 # Value at -2 degree celsius (s^-1 Pa^-3) per second 


# Define length of glacier, x direction divided into individual ice-columns dx
dx = np.linspace(0,1000,10,endpoint=True) # Every 10 meter
x = np.arange(0,1000,10)   # start,stop,step
# x = np.arange(0,200,1)
# y = np.sin(x)
# -0.4x^2+1.1x+25
y = -0.0004*x**2+0.11*x+300
print(len(y))
plt.grid(True,linestyle='dotted')

plt.plot(y,'-')

plt.show()

print((x))

# y = mx+c

# dH = np.gradient(y)
# print((dH))

hx_dict = dict(zip(x,y))
print(hx_dict)

dH = []
dX = []
for hx in hx_dict:
    dx = hx
    dh = (hx_dict[hx])
    dX.append(dx)
    dH.append(dh)
    
    print('dH=%s : dX=%s'%(dh,dx))
    # dhdx = np.diff(dH)/np.diff(dX)
    # print(dhdx)

diff_dhdx = np.diff(dH)/np.diff(dX)
print(diff_dhdx)

dh_dhdx = dict(zip(y,diff_dhdx))
print(dh_dhdx)
# for i in x:
#     dHdx.append(diff(dH)/diff(i))

# print(dHdx)

# Flux = (Diffusivity) x (dh/dx) 
flux_tot = []
for dhdx in dh_dhdx:
    flux = (A*dhdx**(n+2)*dh_dhdx[dhdx]**(n-1)*dh_dhdx[dhdx])
    flux_tot.append(flux)



for H in y:
    flux = (Afl*H**(n+2)*dhdx**(n-1)*dhdx)



def flux():
    flux_in = 
    flux_out = 
    tot_flux = flux_in-flux_out
    return tot_flux

dh_dx = diff(y_savgol)/diff(x)
print('dh_dx list',dh_dx)

def stress_(p,g,H):
    stress = p*g*(H)#*np.sin(theta)
    return stress

def continuity_(A,H,n,stress,dh_dx):
    depth_avg_U = (-1)*(2*A*H/(n+2))*(stress**n)*dh_dx
    return depth_avg_U   


depth_vel=[]
for dhdx in dh_dx:
    # for y in y_savgol:
    stress = stress_(p,g,H)
    U = continuity_(A,H,n,stress,dhdx)
    print('Depth-Avg',U)
    depth_vel.append(U)
plt.plot(depth_vel)

