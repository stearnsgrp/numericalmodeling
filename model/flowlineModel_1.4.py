#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#####################################
# Simple Flowline Model of a glacier#
#####################################

'''
@author: Siddharth Shankar

                        ----------------------------------------
                          Inputs: (Gravity(g), Ice density(p), 
                                  Bed length(x), Bed slope,   
                                  Flow Parameters: n, A)      
                        ----------------------------------------
                                            |
                                            |
                                            V
                            ---------------------------------
                               Accumulation: (Uniform SMB)   
                            ---------------------------------
                                            |
                                            |
                                            V
                            ---------------------------------
                               Continuity Eqn: (dh/dt)        
                            ---------------------------------

'''

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from numpy import diff
import scipy.integrate as integrate
from scipy.interpolate import make_interp_spline, BSpline
from scipy.signal import savgol_filter
from scipy.stats import norm
import matplotlib.animation as ani
import plotly.express as px


g = 9.81 # m/s^2
p = 917 # kg/m^3
n = 3
A = 1.7*10E-24 # Value at -2 degree celsius (s^-1 Pa^-3) per second 


# X: Glacier length km
x = 100
# No of columns 
no_cols = 100

# dx = x/no_cols in meters
dx = (x/no_cols)*1000

print(dx)

A_fl = (2*A*(p*g)**n)/(n+2)

print(A_fl)

# tot_ice_columns = 1000 # number of columns making the entire glacier
# glacier_length = 10000 # in meters

# dx = (glacier_length/tot_ice_columns)
# print(dx)

x = np.linspace(0,10000,1000,dtype=int)

print(x)

dx = (x[1]-x[0])

# Create initial smb surface same shape as length of glacier as input is needed at each point

smb = np.zeros(np.shape(x))
initial_surface = np.zeros(np.shape(x))
print(initial_surface)


for i in range(0,100):
    smb[i] = (2-(i/(0.6*100))**3)*0.3
    initial_surface[i] = ((-1*((i/100))**2)*100)+100

# Uniform surface mass balance flux
smb_Flx = smb*dx

# plt.plot(smb)
# plt.show()

# Function to calculate flux
def flux(H,dhdx):
    D = A_fl*(H**(n+2))*(np.abs(dhdx)**(n-1))
    flux = D*dhdx
    return flux

# Input flux
def inFlux(initial_surface):
    # Create an initial level with same shape as length of the glacier
    in_Flux = np.zeros(np.shape(x))
    for i in range(len(x)):
        if(i!=0):
            in_Flux[i]=initial_surface[i-1]-initial_surface[i] #Update glacier surface which was initially zero
        else:
            continue
    in_Flux = in_Flux/dx
    return in_Flux

# Out flux
def outFlux(initial_surface):
    
    '''
    # Calculate difference between consecutive elements 
    of the array:'inital_surface'
    '''
    
    out_Flux = np.diff(initial_surface)
    out_Flux=np.insert(out_Flux,len(out_Flux),0,axis=0)
    out_Flux = out_Flux/dx
    return out_Flux


# Time-step for x i.e 0 to 10 km

dt = 0.5
new_surface = []
time = np.zeros(np.shape(x))

'''
Test this plot part in animation
'''
m,b = 0.000,0
y = (m*x)+b

# plt.ion()
# fig, ax = plt.subplots()
# base = ax.plot(x,y*-1)
# surf1, = ax.plot(x,initial_surface)

# timeText = 0
# yearCount = ax.text(7000,200,'Years\n'+str(timeText))

for i in range(0,10000):
    inFlx = flux(initial_surface,inFlux(initial_surface))

    outFlx = flux(initial_surface,outFlux(initial_surface))

    tot_Flx = inFlx+outFlx+smb_Flx

    #Set initial_surface to zero when it is negative
    initial_surface[initial_surface < 0] = 0

    initial_surface = initial_surface+((tot_Flx)/dx)*dt
    
    new_surface.append(initial_surface)
    # time=time+dt

    # plt.plot(initial_surface)
    # plt.show()
    
    
    # surf1.set_xdata(x)
    # surf1.set_ydata(initial_surface)
    # timeText += dt
    # yearCount.set_text('Years\n'+'{:.2f}'.format(timeText))
       
    # fig.canvas.draw()
    # fig.canvas.flush_events()

# fig.write_html("file.html")

# import pandas as pd

# df = pd.DataFrame(data = new_surface)
# fig=px.scatter(df, x=x, y=new_surface)
# df.columns=['Glacier_surface']

# fig=px.scatter(df, x=x, y='Glacier_surface',animation_frame=time)

# fig.write_html("file3.html")



# df = pd.DataFrame(data = new_surface)

