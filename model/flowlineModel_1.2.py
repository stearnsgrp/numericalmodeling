#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#####################################
# Simple Flowline Model of a glacier#
#####################################

'''
@author: Siddharth Shankar

                        ----------------------------------------
                          Inputs: (Gravity(g), Ice density(p), 
                                  Bed length(x), Bed slope,   
                                  Flow Parameters: n, A)      
                        ----------------------------------------
                                            |
                                            |
                                            V
                            ---------------------------------
                               Accumulation: (Uniform SMB)   
                            ---------------------------------
                                            |
                                            |
                                            V
                            ---------------------------------
                               Continuity Eqn: (dh/dt)        
                            ---------------------------------

'''

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from numpy import diff
import scipy.integrate as integrate
from scipy.interpolate import make_interp_spline, BSpline
from scipy.signal import savgol_filter
from scipy.stats import norm

g = 9.81 # m/s^2
p = 917 # kg/m^3
n = 3
A = 1.7*10E-24 # Value at -2 degree celsius (s^-1 Pa^-3) per second 


# X: Glacier length km
x = 100
# No of columns 
no_cols = 100

# dx = x/no_cols in meters
dx = (x/no_cols)*1000

print(dx)

A_fl = (2*A*(p*g)**n)/(n+2)

print(A_fl)

tot_ice_columns = 1000 # number of columns making the entire glacier
glacier_length = 10000 # in meters

dx = (glacier_length/tot_ice_columns)
print(dx)

x = np.linspace(0,10000,100,endpoint=True,dtype=int)

# y1= np.linspace(325,300,250,endpoint=True)

y2 = np.linspace(300,290,250,endpoint=True)

y3 = np.linspace(275,225,250,endpoint = True)

y4 = np.linspace(225,50,250,endpoint=True)

y = np.concatenate((y2,y3,y4))

y_savgol = savgol_filter(y,299,3)

y = np.arange(0,310,10)
print(y[::-1].sort())

# initial_height = []
# for i in y:
#     if(i<290):
#         y = np.linspace(i,i+1,100,endpoint=True)
#         initial_height.append(y)
#     elif(i==290):
#         y = np.linspace(i,300,100,endpoint=True)
#         initial_height.append(y)
#     else:
#         print('not within valid range, check y value')
        

# print(initial_height)


y_savgol = savgol_filter(initial_height,99,3)

y_savgol = ((y_savgol[::-1]).sort())

plt.grid(True,linestyle='dotted')
plt.plot(y_savgol,'-')

plt.show()




x = np.arange(0,1000,10)   # start,stop,step
y = -0.0004*x**2+0.11*x+300


upstream_flux

down_flux


def flux()




new_H = []
for num,i in enumerate(y):
    print(num)
    if(num>0 and num<=99):
        dH = y[num]-y[num-1]
        print(dH)
        D = A_fl*(dH**(n+2))*(dH/dx)
        flux = D*(dH/dx)
        print('Flux:',flux)
        new_dH = dH+(flux/(dH))
        new_H.append(new_dH)
    else:
        dH=300
        continue


plt.plot(new_H)
plt.show()

# Using -ve for upglacier flux and +ve for downglacier flux

def flux(y):
    dH = y[num]-y[num-1]
    print(dH)
    D = A_fl*(dH**(n+2))*(dH/dx)
    flux = D*(dH/dx)
    print('Flux:',flux)
    return flux

for num,i in enumerate(y):
    print(num)
    if(num>0 and num<=99):
        flux()
    else:
        continue

    
































# Define length of glacier, x direction divided into individual ice-columns dx
dx = np.linspace(0,1000,10,endpoint=True) # Every 10 meter
x = np.arange(0,1000,10)   # start,stop,step
# x = np.arange(0,200,1)
# y = np.sin(x)
# -0.4x^2+1.1x+25
y = -0.0004*x**2+0.11*x+300
print(len(y))
plt.grid(True,linestyle='dotted')

plt.plot(y,'-')

plt.show()

print((x))

# y = mx+c

# dH = np.gradient(y)
# print((dH))

hx_dict = dict(zip(x,y))
print(hx_dict)

dH = []
dX = []
for hx in hx_dict:
    dx = hx
    dh = (hx_dict[hx])
    dX.append(dx)
    dH.append(dh)
    
    print('dH=%s : dX=%s'%(dh,dx))
    # dhdx = np.diff(dH)/np.diff(dX)
    # print(dhdx)

diff_dhdx = np.diff(dH)/np.diff(dX)
print(diff_dhdx)

dh_dhdx = dict(zip(y,diff_dhdx))
print(dh_dhdx)
# for i in x:
#     dHdx.append(diff(dH)/diff(i))

# print(dHdx)

# Flux = (Diffusivity) x (dh/dx) 
flux_tot = []
for dhdx in dh_dhdx:
    flux = (A*dhdx**(n+2)*dh_dhdx[dhdx]**(n-1)*dh_dhdx[dhdx])
    flux_tot.append(flux)



for H in y:
    flux = (Afl*H**(n+2)*dhdx**(n-1)*dhdx)



def flux():
    flux_in = 
    flux_out = 
    tot_flux = flux_in-flux_out
    return tot_flux

dh_dx = diff(y_savgol)/diff(x)
print('dh_dx list',dh_dx)

def stress_(p,g,H):
    stress = p*g*(H)#*np.sin(theta)
    return stress

def continuity_(A,H,n,stress,dh_dx):
    depth_avg_U = (-1)*(2*A*H/(n+2))*(stress**n)*dh_dx
    return depth_avg_U   


depth_vel=[]
for dhdx in dh_dx:
    # for y in y_savgol:
    stress = stress_(p,g,H)
    U = continuity_(A,H,n,stress,dhdx)
    print('Depth-Avg',U)
    depth_vel.append(U)
plt.plot(depth_vel)

