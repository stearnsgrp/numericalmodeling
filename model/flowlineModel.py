#!/usr/bin/env python3
# -*- coding: utf-8 -*-
####################################
# Simple Flowline Model of a glacier
####################################

'''
@author: Siddharth Shankar

Inputs Bed Slope, Length, 

Accumulation: SMB, No basal melt, surface temperature is uniform

Flow: Glenn's Flow Law
    Parameters of Glenn's Flow Law

Evolution through time: Continuity equation

'''

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from numpy import diff
import scipy.integrate as integrate
from scipy.interpolate import make_interp_spline, BSpline
from scipy.signal import savgol_filter


A=27*10e-25 # Cuffey & Patterson
theta = 1 # theta
BED_LENGTH = 100
SURFACE_TEMP = 5
H = 300 # h
# HEIGHT_ABOVE_BED = 200 # z
rho = 917 # rho
g = 9.81 # gravity
n = 3 # 

'''
dh/dx
driving stress
Depth Averaged Velocity through continuity equation
'''

'''
Define initial geometry
'''

tot_ice_columns = 1000 # number of columns making the entire glacier
glacier_length = 10000 # in meters

dx = (glacier_length/tot_ice_columns)
print(dx)



x = np.linspace(0,1000,1000,endpoint=True,dtype=int)

# y1= np.linspace(325,300,250,endpoint=True)

y2 = np.linspace(300,290,250,endpoint=True)

y3 = np.linspace(275,225,250,endpoint = True)

y4 = np.linspace(225,50,250,endpoint=True)

y = np.concatenate((y2,y3,y4))

y_savgol = savgol_filter(y,299,3)




print(y_savgol)

plt.grid(True,linestyle='dotted')
plt.plot(y_savgol,'-')

plt.show()


dh_dx = diff(y_savgol)/diff(x)
print('dh_dx list',dh_dx)

def stress_(rho,g,H):
    stress = rho*g*(H)#*np.sin(theta)
    return stress

def continuity_(A,H,n,stress,dh_dx):
    depth_avg_U = (-1)*(2*A*H/(n+2))*(stress**n)*dh_dx
    return depth_avg_U   


depth_vel=[]
for dhdx in dh_dx:
    # for y in y_savgol:
    stress = stress_(rho,g,H)
    U = continuity_(A,H,n,stress,dhdx)
    print('Depth-Avg',U)
    depth_vel.append(U)
plt.plot(depth_vel)











