#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#####################################
# Simple Flowline Model of a glacier#
#####################################

'''
@author: Siddharth Shankar

                        ----------------------------------------
                          Inputs: (Gravity(g), Ice density(p), 
                                  Bed length(x), Bed slope,   
                                  Flow Parameters: n, A)      
                        ----------------------------------------
                                            |
                                            |
                                            V
                            ---------------------------------
                               Accumulation: (Uniform SMB)   
                            ---------------------------------
                                            |
                                            |
                                            V
                            ---------------------------------
                               Continuity Eqn: (dh/dt)        
                            ---------------------------------

'''

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from numpy import diff
import scipy.integrate as integrate
from scipy.interpolate import make_interp_spline, BSpline
from scipy.signal import savgol_filter
from scipy.stats import norm
import matplotlib.animation as ani
import plotly.express as px
from celluloid import Camera
import random
import plotly
import plotly.offline as py
import plotly.graph_objs as go

g = 9.81 # m/s^2
p = 917 # kg/m^3
n = 3
A = 1.7*10E-16  # Value at -2 degree celsius (s^-1 Pa^-3) per second 



A_fl = (2*A*(p*g)**n)/(n+2)

x = np.linspace(0,100000,1000,dtype=int)

dx = (x[1]-x[0])

# Create initial smb surface same shape as length of glacier as input is needed at each point

smb = np.zeros(np.shape(x))
initial_surface = np.zeros(np.shape(x)) # Constant initial surface
print(initial_surface)

'''
for i in range(0,100):
    smb[i] = (2-(i/(0.6*100))**3)*0.3 #0.3 SMB constant vs varying
    # initial_surface[i] = ((-1*((i/100))**2)*100)+100 constant vs varying

print(initial_surface)
'''

# Uniform surface mass balance flux
smb_Flx = 0.3*dx
print(smb_Flx)
# Function to calculate flux
def flux(H,dhdx):
    D = A_fl*(H**(n+2))*((dhdx)**(n-1))
    flux = D*dhdx
    return flux


# Input flux
def inFlux(initial_surface):
    # Create an initial level with same shape as length of the glacier
    in_Flux = np.zeros(np.shape(x))
    for i in range(len(x)):
        if(i!=0):
            in_Flux[i]=initial_surface[i-1]-initial_surface[i] #Update glacier surface which was initially zero
        else:
            continue
    in_Flux = in_Flux/dx
    return in_Flux


# Out flux
def outFlux(initial_surface):
    
    '''
    # Calculate difference between consecutive elements 
    of the array:'inital_surface'
    '''
    
    out_Flux = np.diff(initial_surface)
    out_Flux=np.insert(out_Flux,len(out_Flux),0,axis=0)
    out_Flux = out_Flux/dx
    return out_Flux


# Time-step for x i.e 0 to 10 km

'''
Test this plot part in animation

'''

fig, ax = plt.subplots()
dt =  1#/(2*365)#year

time_ = 0
df_list = []

for i in range(1500):
    inFlx = flux(initial_surface,inFlux(initial_surface))

    outFlx = flux(initial_surface,outFlux(initial_surface))

    tot_Flx = inFlx+outFlx+smb_Flx
    # plt.ylim(0,1000)
    # plt.xlabel('Glacier Length(meters)')
    # plt.ylabel('Glacier Height (meters)')
    # plt.title('Glacier Flowline model')
    #Set initial_surface to zero when it is negative
    initial_surface[initial_surface < 0] = 0

    initial_surface = np.array(initial_surface+((tot_Flx)/dx)*dt)
    time_+=dt
    df = pd.DataFrame()
    df['surface'] = initial_surface
    df['time'] = time_
    df_list.append(df)
    df.index.name = 'id'
    df['x'] = x


df_new = pd.DataFrame(np.concatenate(df_list),columns=df_list[0].columns)
fig = px.area(df_new, x=df_new.x, y=df_new.surface, animation_frame="time",range_y=[0,1000],
        labels=dict(x = 'Glacier length (meters)',
        surface = 'Glacier Surface Elevation (meters)'),
        color_discrete_sequence=px.colors.qualitative.D3
        )
fig.update_layout(
    title="Simple Flowline Model",
    font=dict(
        family="Palatine Linotype",
        size=14,
        color="Black"
    )
)

fig.update_xaxes(nticks=50,showline=True, linewidth=2, linecolor='grey',mirror=True)
fig.update_yaxes(nticks=50,showline=True, linewidth=2, linecolor='grey',mirror=True)

fig.show()